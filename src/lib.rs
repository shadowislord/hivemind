#![feature(hash_extract_if)]
#![feature(ip)]
// #![warn(clippy::all, clippy::pedantic)]

//! Hivemind is a peer to peer protocol simulator written in Rust
//!
//! Example of a basic protocol that does nothing:
//! ```rust
//! # use std::rc::Rc;
//! # use rand::rngs::StdRng;
//! # use hivemind::{Address, Context, Protocol};
//! # use async_trait::async_trait;
//! #
//! struct TestProto;
//!
//! #[async_trait(?Send)]
//! impl Protocol for TestProto {
//!     type Request = String;
//!     type Response = String;
//!     type Config = String;
//!
//!     fn create_data(_context: Context<String, String>, _rng: &mut StdRng, _address: Address, config: &String) -> Self {
//!         assert_eq!(config, "config");
//!         Self
//!     }
//!
//!     async fn initialize(self: Rc<Self>, _: Context<String, String>) {}
//!
//!     async fn handle_request(
//!         self: &Rc<Self>,
//!         _: &Context<String, String>,
//!         _: Address,
//!         request: Self::Request,
//!     ) -> Self::Response {
//!         assert_eq!(request, "ping");
//!         "pong".to_owned()
//!     }
//! }
//!
//! # use tracing::Level;
//! # use tracing_subscriber::fmt::format::FmtSpan;
//! # use hivemind::{Simulation, SimulationConfig};
//! #
//! # fn main() {
//! tracing_subscriber::fmt()
//!         .with_max_level(Level::INFO)
//!         .with_span_events(FmtSpan::NONE)
//!         .init();
//!
//! let mut sim = Simulation::<TestProto>::new(SimulationConfig::default(), "config".to_owned());
//! sim.tick();
//! # }
//! ```

pub use futures_lite::future::yield_now;

pub use config::SimulationConfig;
pub use context::Context;
pub use proto::Protocol;
pub use simulation::Simulation;
pub use tls::current_time;
pub use tls::is_context_valid;
pub use tls::sleep;
pub use tls::spawn;
pub use tls::with_simulation_rng;

pub use crate::address::Address;
pub use crate::rpc::Timeout;
pub use crate::utils::RngExt;

type Cookie = u32;

mod address;
mod context;
mod event;
mod executor;
pub mod futures;
mod oneshot_internal;
mod proto;
mod rpc;
mod simulation;
mod task;
mod tls;

mod address_generator;
pub(crate) mod buildhasher;
mod config;
pub mod join;
pub mod oneshot;
pub mod stats;
pub mod time;
mod utils;
mod waker;
