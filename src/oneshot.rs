//! oneshot channels to send data around.

use std::fmt;
use std::fmt::Debug;
use std::future::Future;
use std::pin::Pin;
use std::rc::Rc;
use std::task::{Context, Poll, Waker};

use once_cell::unsync::OnceCell;

#[derive(Debug)]
pub struct Sender<T> {
    inner: Rc<Inner<T>>,
}

#[derive(Debug)]
struct Inner<T> {
    cell: OnceCell<T>,
    rx_task: OnceCell<Waker>,
}

impl<T> Sender<T> {
    pub fn send(self, t: T) {
        if self.inner.cell.set(t).is_err() {
            unreachable!();
        }
        if let Some(waker) = self.inner.rx_task.get() {
            waker.wake_by_ref();
        }
    }
}

impl<T> Drop for Sender<T> {
    fn drop(&mut self) {
        if let Some(waker) = self.inner.rx_task.get() {
            waker.wake_by_ref();
        }
    }
}

#[derive(Debug)]
pub struct Receiver<T> {
    inner: Rc<Inner<T>>,
}

#[derive(Debug, Eq, PartialEq)]
pub enum TryRecvError {
    Empty,
    Closed,
}

impl fmt::Display for TryRecvError {
    fn fmt(&self, fmt: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            TryRecvError::Empty => write!(fmt, "channel empty"),
            TryRecvError::Closed => write!(fmt, "channel closed"),
        }
    }
}

impl std::error::Error for TryRecvError {}

#[derive(Debug)]
#[must_use = "futures do nothing unless you `.await` or poll them"]
pub struct ReceiverFuture<T> {
    inner: Rc<Inner<T>>,
}

impl<T> ReceiverFuture<T> {
    pub(crate) fn is_finished(&self) -> bool {
        Rc::strong_count(&self.inner) == 1 && Rc::weak_count(&self.inner) == 0
    }
}

impl<T> Future for ReceiverFuture<T> {
    type Output = Result<T, TryRecvError>;

    fn poll(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        if let Some(inner) = Rc::get_mut(&mut self.inner) {
            if let Some(result) = inner.cell.take() {
                Poll::Ready(Ok(result))
            } else {
                Poll::Ready(Err(TryRecvError::Closed))
            }
        } else {
            if let Some(waker) = self.inner.rx_task.get() {
                assert!(
                    waker.will_wake(cx.waker()),
                    "different waker here than the one registered, big problems!"
                );
            } else if self.inner.rx_task.set(cx.waker().clone()).is_err() {
                unreachable!();
            }
            Poll::Pending
        }
    }
}

impl<T> Receiver<T> {
    pub fn try_recv(&mut self) -> Result<T, TryRecvError> {
        if let Some(inner) = Rc::get_mut(&mut self.inner) {
            if let Some(result) = inner.cell.take() {
                Ok(result)
            } else {
                Err(TryRecvError::Closed)
            }
        } else {
            Err(TryRecvError::Empty)
        }
    }

    pub fn recv(self) -> ReceiverFuture<T> {
        ReceiverFuture { inner: self.inner }
    }
}

#[must_use]
pub fn channel<T>() -> (Sender<T>, Receiver<T>) {
    let inner = Rc::new(Inner {
        cell: OnceCell::new(),
        rx_task: OnceCell::new(),
    });
    let tx = Sender {
        inner: inner.clone(),
    };
    let rx = Receiver { inner };
    (tx, rx)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn send_and_recv() {
        let (tx, mut rx) = channel();
        assert_eq!(rx.try_recv(), Err(TryRecvError::Empty));
        tx.send("hello");
        assert_eq!(rx.try_recv(), Ok("hello"));
    }

    #[test]
    fn recv_twice() {
        let (tx, mut rx) = channel();
        tx.send("hello");
        assert_eq!(rx.try_recv(), Ok("hello"));
        assert_eq!(rx.try_recv(), Err(TryRecvError::Closed));
    }

    #[test]
    fn closed_after_drop() {
        let (tx, mut rx) = channel::<u32>();
        assert_eq!(rx.try_recv(), Err(TryRecvError::Empty));
        drop(tx);
        assert_eq!(rx.try_recv(), Err(TryRecvError::Closed));
    }
}
