//! HashMap hasher with fixed keys to allow simulation predictability.

// TODO: consider making these types public so they can be used in user code

use siphasher::sip::SipHasher13;
use std::collections::{HashMap, HashSet};
use std::hash::{BuildHasherDefault, Hasher};

#[derive(Clone, Debug)]
pub(crate) struct SimHasher(SipHasher13);

impl Default for SimHasher {
    fn default() -> Self {
        SimHasher(SipHasher13::new())
    }
}

impl Hasher for SimHasher {
    #[inline]
    fn finish(&self) -> u64 {
        self.0.finish()
    }

    #[inline]
    fn write(&mut self, msg: &[u8]) {
        self.0.write(msg)
    }
}

pub(crate) type SimBuildHasher = BuildHasherDefault<SimHasher>;

#[allow(dead_code)]
pub(crate) type SimHashMap<K, V> = HashMap<K, V, SimBuildHasher>;

#[allow(dead_code)]
pub(crate) type SimHashSet<T> = HashSet<T, SimBuildHasher>;
