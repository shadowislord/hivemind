use crate::executor::TaskId;
use once_cell::unsync::OnceCell;
use std::fmt;
use std::fmt::{Debug, Formatter};
use std::future::Future;
use std::pin::Pin;
use std::rc::Rc;
use std::task::{Context, Poll};

pub struct Task {
    future: Pin<Box<dyn Future<Output = ()> + 'static>>,
    task_id: Rc<OnceCell<TaskId>>,
}

impl Debug for Task {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "Task(...)")
    }
}

impl Task {
    pub fn new_boxed(future: Pin<Box<dyn Future<Output = ()> + 'static>>) -> Task {
        Task {
            future,
            task_id: Rc::new(OnceCell::new()),
        }
    }

    pub(crate) fn set_id(&mut self, id: TaskId) {
        self.task_id.set(id).expect("unable to set id again");
    }

    pub(crate) fn get_id_ref(&self) -> Rc<OnceCell<TaskId>> {
        Rc::clone(&self.task_id)
    }

    pub(crate) fn is_same_id(&self, task_id_ref: &Rc<OnceCell<TaskId>>) -> bool {
        Rc::ptr_eq(&self.task_id, task_id_ref)
    }

    pub fn poll(&mut self, context: &mut Context) -> Poll<()> {
        assert!(
            self.task_id.get().is_some(),
            "unable to run task as it has no ID"
        );
        self.future.as_mut().poll(context)
    }
}
