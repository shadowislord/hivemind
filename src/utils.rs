use rand::Rng;
use rand_distr::StandardNormal;
use std::collections::{BinaryHeap, VecDeque};

pub(crate) trait BinaryHeapExt<T> {
    /// Pop an element if the given closure returns true for it
    fn pop_if<F>(&mut self, f: F) -> Option<T>
    where
        F: Fn(&T) -> bool;
}

impl<T: Ord> BinaryHeapExt<T> for BinaryHeap<T> {
    fn pop_if<F>(&mut self, f: F) -> Option<T>
    where
        F: Fn(&T) -> bool,
    {
        if let Some(el) = self.peek() {
            if f(el) {
                return self.pop();
            }
        }
        None
    }
}

pub(crate) trait VecDequeExt<T> {
    /// Pop the front element if the given closure returns true for it
    fn pop_front_if<F>(&mut self, f: F) -> Option<T>
    where
        F: Fn(&T) -> bool;
}

impl<T> VecDequeExt<T> for VecDeque<T> {
    fn pop_front_if<F>(&mut self, f: F) -> Option<T>
    where
        F: Fn(&T) -> bool,
    {
        if let Some(el) = self.front() {
            if f(el) {
                return self.pop_front();
            }
        }
        None
    }
}

pub trait RngExt {
    /// Returns a random integer from 0 to limit except for "except"
    ///
    /// E.g., for limit = 5, except = 2, the following values may be returned: 0, 1, 3, 4.
    fn usize_except(&mut self, limit: usize, except: usize) -> usize;

    /// Generate a number following the standard gaussian distribution
    fn f32_gaussian(&mut self) -> f32;

    /// Generate a 128-bit integer
    fn u128(&mut self) -> u128;
}

impl<T> RngExt for T
where
    T: Rng,
{
    fn usize_except(&mut self, limit: usize, except: usize) -> usize {
        assert!(except < limit);

        let mut random = self.gen_range(0..limit - 1);
        random = if random < except { random } else { random + 1 };

        assert_ne!(random, except);
        assert!(random < limit);

        random
    }

    fn f32_gaussian(&mut self) -> f32 {
        self.sample(StandardNormal)
    }

    fn u128(&mut self) -> u128 {
        let mut array = [0_u8; 16];
        self.fill_bytes(&mut array);
        u128::from_le_bytes(array)
    }
}

#[cfg(test)]
mod tests {
    use crate::utils::RngExt;
    use rand::rngs::StdRng;
    use rand::SeedableRng;

    #[test]
    fn gaussian() {
        let mut rng = StdRng::seed_from_u64(0);
        for _ in 0..100 {
            println!("{}", rng.f32_gaussian());
        }
    }

    #[test]
    fn lognormal() {
        let mu = 5.;
        let sigma = 0.6;
        let mut rng = StdRng::seed_from_u64(0);
        for _ in 0..10_000 {
            let value = (rng.f32_gaussian() * sigma + mu).exp();
            if value > 1000. {
                println!("{}", value);
            }
        }
    }
}
