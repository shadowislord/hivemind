//! Various utility futures that you can use.

use crate::tls;
use futures_lite::Stream;
use pin_project_lite::pin_project;
use std::future::Future;
use std::pin::Pin;
use std::task::{Context, Poll};
pub use tls::SleepFuture;

pin_project! {
    /// A list of futures that can be polled together
    pub struct FuturesUnordered<Fut: Future> {
        #[pin]
        pending: Box<[Option<Fut>]>,
        ready: Vec<Fut::Output>,
        len: usize,
    }
}

impl<Fut: Future> FuturesUnordered<Fut> {
    /// Create a new futures unordered with the given capacity
    ///
    /// The capacity is the limit of how many futures can be put in
    /// Use [`Self::is_full`] to determine if there's no more room
    pub fn new(capacity: usize) -> Self {
        let mut vec = Vec::with_capacity(capacity);
        for _ in 0..capacity {
            vec.push(None);
        }
        Self {
            pending: vec.into_boxed_slice(),
            ready: vec![],
            len: 0,
        }
    }

    /// Push a new future to be processed
    ///
    /// # Panics
    ///
    /// If the capacity has been exceeded
    /// Use [`Self::is_full`] to determine if there's no more room
    pub fn push(&mut self, fut: Fut) {
        for opt_fut in self.pending.iter_mut() {
            if opt_fut.is_none() {
                self.len += 1;
                if opt_fut.replace(fut).is_some() {
                    unreachable!();
                }
                return;
            }
        }
        panic!("no room");
    }

    // TODO: keep track of size using instance variable instead of counting
    //       right now it's fine since most FuturesUnordered are small

    /// Return the number of futures that can be added
    pub fn available(&self) -> usize {
        self.pending.len() - self.len()
    }

    /// Determine if the futures unordered is full
    pub fn is_full(&self) -> bool {
        self.available() == 0
    }

    /// Return the number of futures that are currently pending
    pub fn len(&self) -> usize {
        debug_assert_eq!(self.len, self.len_slow());
        self.len
    }

    fn len_slow(&self) -> usize {
        self.pending.iter().filter(|n| n.is_some()).count()
    }

    /// Determine if the futures unordered is empty
    pub fn is_empty(&self) -> bool {
        self.len() == 0
    }

    /// Remove all the futures. Pending results are not removed
    pub fn clear(&mut self) {
        for opt_fut in self.pending.iter_mut() {
            *opt_fut = None;
        }
    }
}

impl<Fut: Future> Stream for FuturesUnordered<Fut> {
    type Item = Fut::Output;

    fn poll_next(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Option<Self::Item>> {
        let mut this = self.project();

        let mut has_pending_future = false;
        for opt_fut in this.pending.iter_mut() {
            if let Some(fut) = opt_fut {
                let fut = unsafe { Pin::new_unchecked(fut) };
                if let Poll::Ready(ret) = fut.poll(cx) {
                    this.ready.push(ret);
                    *opt_fut = None;
                    *this.len -= 1;
                } else {
                    has_pending_future = true;
                }
            }
        }

        debug_assert_eq!(has_pending_future, this.pending.iter().any(Option::is_some));

        if let Some(ret) = this.ready.pop() {
            Poll::Ready(Some(ret))
        } else if !has_pending_future {
            Poll::Ready(None)
        } else {
            Poll::Pending
        }
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        let len = self.len();
        (len, Some(len))
    }
}
