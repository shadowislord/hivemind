//! Utility class to show simulated time for tracing log messages

use tracing_subscriber::fmt::format::Writer;
use tracing_subscriber::fmt::time::FormatTime;

use std::cell::Cell;

thread_local! {
    static TIME: Cell<u32> = Cell::new(0);
}

pub struct SimulationTimer;

impl SimulationTimer {
    pub(crate) fn set_time(t: u32) {
        TIME.with(|time| time.set(t));
    }
}

impl FormatTime for SimulationTimer {
    fn format_time(&self, w: &mut Writer<'_>) -> std::fmt::Result {
        let time = TIME.with(Cell::get);
        let millis = time % 1000;
        let mut seconds = time / 1000;
        let mut minutes = seconds / 60;
        let hours = minutes / 60;
        seconds = seconds % 60;
        minutes = minutes % 60;
        write!(
            w,
            "{:02}:{:02}:{:02},{:03}",
            hours, minutes, seconds, millis
        )
    }
}
