use std::net::SocketAddrV4;

/// Represents the address of a node in the network
///
/// For simulation purposes it's hardcoded to IPv4, although in real usage
/// either IPv4 or IPv6 need to be handled
pub type Address = SocketAddrV4;

#[cfg(test)]
pub fn integer_to_address(i: u32) -> SocketAddrV4 {
    let ip = std::net::Ipv4Addr::from(i + 1);
    let port = 1024;
    SocketAddrV4::new(ip, port)
}
