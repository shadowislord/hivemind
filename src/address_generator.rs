use rand::Rng;
use std::collections::HashSet;
use std::net::{Ipv4Addr, SocketAddrV4};

#[derive(Default, Debug)]
pub(crate) struct AddressGenerator {
    generated: HashSet<SocketAddrV4>,
}

impl AddressGenerator {
    fn generate_internal(&mut self, rng: &mut impl Rng) -> SocketAddrV4 {
        let mut ip = Ipv4Addr::from(rng.next_u32());
        while !ip.is_global() {
            ip = Ipv4Addr::from(rng.next_u32());
        }
        let port = rng.gen_range(1024..u16::MAX);
        SocketAddrV4::new(ip, port)
    }

    pub fn generate(&mut self, rng: &mut impl Rng) -> SocketAddrV4 {
        let mut addr = self.generate_internal(rng);
        while !self.generated.insert(addr) {
            addr = self.generate_internal(rng);
        }
        addr
    }
}

#[cfg(test)]
mod tests {
    use crate::address_generator::AddressGenerator;
    use rand::rngs::StdRng;
    use rand::SeedableRng;

    #[test]
    fn can_generate_address() {
        let mut rng = StdRng::seed_from_u64(0);
        let mut addr_gen = AddressGenerator::default();
        let a1 = addr_gen.generate(&mut rng);
        let a2 = addr_gen.generate(&mut rng);
        assert_ne!(a1, a2);
    }
}
