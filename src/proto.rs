use std::fmt::Debug;
use std::rc::Rc;

use async_trait::async_trait;
use rand::rngs::StdRng;

use crate::context::Context;
use crate::Address;

/// All protocols implement this trait to be simulated.
#[async_trait(?Send)]
pub trait Protocol: Sized + Unpin + 'static {
    /// Request message data type
    type Request: Debug + 'static;

    /// Response message data type
    type Response: Debug + 'static;

    /// Configuration that is provided to [`Self::create_data`].
    type Config: 'static;

    /// Create the protocol's data blob.
    ///
    /// Is provided as self in all methods.
    fn create_data(
        context: Context<Self::Request, Self::Response>,
        rng: &mut StdRng,
        address: Address,
        config: &Self::Config,
    ) -> Self;

    /// Runs at bootstrap for every instance of the protocol.
    async fn initialize(self: Rc<Self>, context: Context<Self::Request, Self::Response>);

    /// Handle a request from another node running this protocol.
    ///
    /// The address of the node and the request payload is provided.
    /// This should be an async method returning the response after the request has
    /// been processed.
    async fn handle_request(
        self: &Rc<Self>,
        context: &Context<Self::Request, Self::Response>,
        address: Address,
        request: Self::Request,
    ) -> Self::Response;

    /// Get notified of a response to one of your requests.
    fn on_response_received(
        self: &Rc<Self>,
        _context: &Context<Self::Request, Self::Response>,
        _address: Address,
        _response: &Self::Response,
    ) {
        // (not required to be implemented, so have no-op default)
    }

    /// Get friendly name of this node instance for debugging.
    fn name(&self) -> String {
        "".to_string()
    }

    /// Can the protocol handle this request synchronously?
    /// If so, [`Self::handle_request_sync`] will be called,
    /// otherwise, [`Self::handle_request`] will be called.
    fn support_sync_handle_request(_request: &Self::Request) -> bool {
        false
    }

    /// If [`Self::support_sync_handle_request`] returns true for a request,
    /// then this handler will be called.
    fn handle_request_sync(
        self: &Rc<Self>,
        _context: &Context<Self::Request, Self::Response>,
        _address: Address,
        _request: Self::Request,
    ) -> Self::Response {
        todo!()
    }
}
