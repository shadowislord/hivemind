use std::task::{RawWaker, RawWakerVTable, Waker};
use std::thread;
use tracing::warn;
use std::cell::Cell;

use slotmap::{Key, KeyData};

use crate::executor::TaskId;

fn taskid_clone(waker: *const ()) -> RawWaker {
    RawWaker::new(waker, &TASKID_VTABLE)
}

#[cfg(target_pointer_width = "64")]
pub(crate) fn get_task_id(waker: *const ()) -> TaskId {
    KeyData::from_ffi(waker as u64).into()
}

// TODO: fix support for running on 32-bit platforms

#[cfg(not(target_pointer_width = "64"))]
pub(crate) fn get_task_id(waker: *const ()) -> TaskId {
    panic!("sorry, but this library won't work for you...");
}

fn taskid_wake(waker: *const ()) {
    taskid_wake_by_ref(waker);
}

thread_local! {
    static WAKE_PANIC: Cell<bool> = Cell::new(false);
}

fn taskid_wake_by_ref(waker: *const ()) {
    let tid = get_task_id(waker);

    if !thread::panicking() {
        // problem: if this is dropped outside of the context, we'll get a panic...
        let _result = crate::tls::with_tls_option(move |tls| {
            tls.wake_up.push(tid);
        });

        // XXX: remove this log message due to too many false positives
        // if result.is_none() {
        //     warn!(
        //         "failed to wake up task {}, probably because executor is being dropped",
        //         tid
        //     );
        // }
    } else {
        // probably the context has been yanked, so just give up
        WAKE_PANIC.with(|c| {
            if !c.get() {
                warn!("cannot wake up task {} because panicking", tid);
                c.set(true);
            }
        });
    }
}

fn taskid_drop(_: *const ()) {
    // nothing to do
}

pub(crate) fn taskid_waker(task_id: TaskId) -> Waker {
    assert!(!task_id.is_null(), "task_id cannot be null");
    let ptr: *const () = task_id.data().as_ffi() as *const ();
    let raw_waker = RawWaker::new(ptr, &TASKID_VTABLE);
    unsafe { Waker::from_raw(raw_waker) }
}

const TASKID_VTABLE: RawWakerVTable =
    RawWakerVTable::new(taskid_clone, taskid_wake, taskid_wake_by_ref, taskid_drop);

#[cfg(test)]
mod tests {
    use rand::rngs::StdRng;
    use rand::SeedableRng;
    use std::mem::{size_of, transmute};

    use crate::address::integer_to_address;
    use slotmap::KeyData;

    use crate::executor::ExecutorThreadLocal;
    use crate::tls::{load_tls, unload_tls};
    use crate::waker::taskid_waker;

    #[test]
    fn can_encode_key_data() {
        assert_eq!(size_of::<u64>(), size_of::<*const ()>(),)
    }

    #[test]
    fn wake() {
        let task_id = KeyData::from_ffi(1).into();
        let mut exec = ExecutorThreadLocal::new(StdRng::seed_from_u64(0), integer_to_address(0));
        load_tls(unsafe { transmute(&mut exec) });
        let waker = taskid_waker(task_id);
        waker.wake();
        unload_tls();
        assert_eq!(exec.wake_up, vec![task_id]);
    }

    #[test]
    fn wake_by_ref() {
        let task_id = KeyData::from_ffi(1).into();
        let mut exec = ExecutorThreadLocal::new(StdRng::seed_from_u64(0), integer_to_address(0));
        load_tls(unsafe { transmute(&mut exec) });
        let waker = taskid_waker(task_id);
        waker.wake_by_ref();
        unload_tls();
        assert_eq!(exec.wake_up, vec![task_id]);
    }

    #[test]
    fn wake_no_context() {
        let waker = taskid_waker(KeyData::from_ffi(1).into());
        waker.wake();
    }
}
