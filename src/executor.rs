use std::cmp::Ordering;
use std::collections::{BinaryHeap, VecDeque};
use std::fmt;
use std::future::Future;
use std::pin::Pin;
use std::rc::Rc;

use tracing::{span, trace, warn, Level, Span};

use fnv::FnvHashSet;
use rand::rngs::StdRng;
use rand::SeedableRng;
use slotmap::SlotMap;
use slotmap::{new_key_type, Key};

use crate::buildhasher::SimHashMap;
use crate::context::{Context, ContextLock};
use crate::join::{into_task_and_join_handle, JoinHandle};
use crate::oneshot_internal::Sender;
use crate::proto::Protocol;
use crate::rpc::RpcMessage;
use crate::simulation::SimulationInContext;
use crate::task::Task;
use crate::utils::{BinaryHeapExt, VecDequeExt};
use crate::waker::taskid_waker;
use crate::{Address, Cookie};

new_key_type! { pub struct TaskId; }

/// This structure is placed into the context when running tasks.
///
/// Tasks have direct access to the data in here when using the context.
#[derive(Debug)]
pub(crate) struct ExecutorInContext<A> {
    pub(crate) pending: SimHashMap<(Address, Cookie), Sender<A>>,
    pub(crate) timeouts: VecDeque<(Address, Cookie, u32)>,
    pub(crate) next_cookie: Cookie,
    pub(crate) friends: Vec<Address>,
    pub(crate) my_address: Address,
    pub(crate) rng: StdRng,
}

/// This is the non generic context data.
///
/// It's stored in a thread local.
#[derive(Debug)]
pub(crate) struct ExecutorThreadLocal {
    pub(crate) spawned: Vec<Task>,
    pub(crate) wake_up: Vec<TaskId>,
    pub(crate) aborted: Vec<TaskId>,
    pub(crate) sleep: Vec<(u32, TaskId)>,
    pub(crate) current_task_id: TaskId,
    pub(crate) current_time: u32,
    pub(crate) rng: StdRng,
    pub(crate) address: Address,
}

impl ExecutorThreadLocal {
    pub(crate) fn new(rng: StdRng, address: Address) -> Self {
        Self {
            spawned: vec![],
            wake_up: vec![],
            aborted: vec![],
            sleep: vec![],
            current_task_id: TaskId::null(),
            current_time: 0,
            rng,
            address,
        }
    }
}

impl<A> ExecutorInContext<A> {
    fn new(rng_seed: u64, address: Address) -> Self {
        Self {
            pending: SimHashMap::default(),
            timeouts: VecDeque::new(),
            next_cookie: 0,
            friends: vec![],
            my_address: address,
            rng: StdRng::seed_from_u64(rng_seed),
        }
    }
}

#[derive(Debug)]
struct SleepingTask {
    time: u32,
    id: TaskId,
}

impl SleepingTask {
    pub fn new(time: u32, id: TaskId) -> SleepingTask {
        Self { time, id }
    }
}

impl PartialEq<Self> for SleepingTask {
    fn eq(&self, other: &Self) -> bool {
        self.id == other.id
    }
}

impl Eq for SleepingTask {}

impl PartialOrd<Self> for SleepingTask {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for SleepingTask {
    fn cmp(&self, other: &Self) -> Ordering {
        other.time.cmp(&self.time)
    }
}

#[derive(Debug)]
pub(crate) struct Executor<P: Protocol> {
    ready: FnvHashSet<TaskId>,
    sleep: BinaryHeap<SleepingTask>,
    protocol: Rc<P>,
    context: Context<P::Request, P::Response>,
    in_context: ExecutorInContext<P::Response>,
    in_tls: ExecutorThreadLocal,
    tasks: SlotMap<TaskId, Task>,
    birth_time: u32,
    death_time: u32,
    sync_request_queue: Vec<(Address, Cookie, P::Request)>,
    // node_name: String,
    span: Option<Span>,
}

impl fmt::Display for TaskId {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let key_data = self.data();
        write!(f, "{}", key_data.as_ffi() & 0xffff)
    }
}

impl<P: Protocol> Executor<P> {
    pub(crate) fn new(
        rng_seed: u64,
        address: Address,
        config: &P::Config,
        birth_time: u32,
        death_time: u32,
    ) -> Self {
        let mut in_context = ExecutorInContext::new(rng_seed, address);
        let context = Context::new();
        let protocol = Rc::new(P::create_data(
            context.clone(),
            &mut in_context.rng,
            address,
            config,
        ));
        let init_task = P::initialize(protocol.clone(), context.clone());
        let node_name = protocol.name();

        let span = if node_name.is_empty() {
            span!(
                Level::INFO,
                "node",
                address = tracing::field::display(address.ip()),
            )
        } else {
            span!(
                Level::INFO,
                "node",
                id = tracing::field::display(node_name),
                address = tracing::field::display(address.ip()),
            )
        };

        let mut exec = Self {
            tasks: SlotMap::with_key(),
            ready: FnvHashSet::default(),
            sleep: BinaryHeap::new(),
            context,
            in_tls: ExecutorThreadLocal::new(StdRng::seed_from_u64(rng_seed + 1), address),
            in_context,
            protocol,
            birth_time,
            death_time,
            span: Some(span),
            sync_request_queue: vec![],
            // node_name,
        };
        exec.spawn_box(init_task);
        exec
    }

    // pub fn address(&self) -> Address {
    //     self.in_context.my_address
    // }

    pub fn protocol(&self) -> &Rc<P> {
        &self.protocol
    }

    pub fn context(&self) -> Context<P::Request, P::Response> {
        self.context.clone()
    }

    #[allow(dead_code)]
    pub fn spawn<T>(&mut self, fut: impl Future<Output = T> + 'static) -> JoinHandle<T>
    where
        T: 'static,
    {
        let (mut task, join_handle) = into_task_and_join_handle(fut, self.in_tls.address);
        let key = self.tasks.insert_with_key(|key| {
            task.set_id(key);
            task
        });
        self.ready.insert(key);
        // simulation.stats.tasks_created += 1;
        join_handle
    }

    pub fn spawn_box(&mut self, fut: Pin<Box<dyn Future<Output = ()> + 'static>>) {
        let key = self.tasks.insert_with_key(|key| {
            let mut task = Task::new_boxed(fut);
            task.set_id(key);
            task
        });
        self.ready.insert(key);
        // simulation.stats.tasks_created += 1;
    }

    pub(crate) fn set_friends(&mut self, friends: Vec<Address>) {
        trace!("{}: got friend {:?}", self.in_context.my_address, friends);
        self.in_context.friends = friends;
    }

    fn handle_answer(&mut self, addr: Address, cookie: Cookie, answer: P::Response) {
        if let Some(ref mut sender) = self.in_context.pending.get_mut(&(addr, cookie)) {
            trace!(
                "{} received response {:?} from {}",
                self.in_context.my_address,
                answer,
                addr
            );

            if let Some(task_id) = sender.send(answer) {
                self.ready.insert(task_id);
            }
        } else {
            warn!("ignoring answer: {:?}", answer);
        }
    }

    pub(crate) fn death_time(&self) -> u32 {
        self.death_time
    }

    pub(crate) fn birth_time(&self) -> u32 {
        self.birth_time
    }

    // pub(crate) fn node_name(&self) -> &str {
    //     &self.node_name
    // }

    /// Enqueues a feature on the executor to handle the given request.
    fn handle_request(&mut self, addr: Address, cookie: Cookie, request: P::Request) {
        trace!(
            "{} received request {:?} from {}",
            self.in_context.my_address,
            request,
            addr
        );

        let context = self.context();

        if P::support_sync_handle_request(&request) {
            self.sync_request_queue.push((addr, cookie, request));
        } else {
            let proto = self.protocol.clone();
            let box_future = Box::pin(async move {
                let answer = P::handle_request(&proto, &context, addr, request).await;
                context.respond(addr, cookie, answer);
            });
            self.spawn_box(box_future);
        }
    }

    pub(crate) fn on_message_received(
        &mut self,
        addr: Address,
        msg: RpcMessage<P::Request, P::Response>,
    ) {
        match msg {
            RpcMessage::Request(cookie, request) => {
                self.handle_request(addr, cookie, request);
            }
            RpcMessage::Answer(cookie, answer) => {
                self.handle_answer(addr, cookie, answer);
            }
        }
    }

    #[allow(unused)]
    pub fn process_until_no_tasks(
        &mut self,
        simulation: &mut SimulationInContext<P::Request, P::Response>,
    ) {
        let mut tick = 1;
        loop {
            if self.tasks.is_empty() {
                break;
            }
            assert!(tick <= 5000, "too many ticks");
            println!("----- time {}", simulation.time);
            self.process(simulation);
            tick += 1;
            simulation.time += 100;
        }
    }

    fn process_messages(&mut self, simulation: &mut SimulationInContext<P::Request, P::Response>) {
        self.in_tls.current_task_id = TaskId::null();

        let mut ctx = self.context.clone();
        let proto = self.protocol.clone();

        let _lock = ContextLock::acquire(
            &mut self.context,
            simulation,
            &mut self.in_context,
            &mut self.in_tls,
        );

        for (address, cookie, request) in self.sync_request_queue.drain(..) {
            let answer = P::handle_request_sync(&self.protocol, &ctx, address, request);
            ctx.respond(address, cookie, answer);
        }

        // XXX: responses may take some time to deliver, so this loop
        //      will end up wasting time...
        let received = ctx.pending().extract_if(|_, v| v.get().is_some());
        for ((address, _), response) in received {
            P::on_response_received(&proto, &self.context, address, response.get().unwrap());
        }
    }

    fn process_timeouts(&mut self, now: u32) {
        while let Some((addr, cookie, _)) = self.in_context.timeouts.pop_front_if(|t| t.2 <= now) {
            if let Some(sender) = self.in_context.pending.remove(&(addr, cookie)) {
                if let Some(task_id) = sender.timeout() {
                    trace!(
                        "timed out waiting for a response from {} (cookie: {})",
                        addr,
                        cookie
                    );
                    self.ready.insert(task_id);
                }
            }
        }
    }

    fn process_sleeping_tasks(&mut self, now: u32) {
        while let Some(task) = self.sleep.pop_if(|t| t.time <= now) {
            self.ready.insert(task.id);
        }
    }

    pub fn disconnect(&mut self) {
        let enter = self.span.take().unwrap().entered();
        trace!("disconnected");
        self.span = Some(enter.exit());
    }

    pub fn protect(&mut self) {
        self.death_time = u32::MAX;
    }

    pub fn run_on<C, T>(
        &mut self,
        simulation: &mut SimulationInContext<P::Request, P::Response>,
        f: C,
    ) -> T
    where
        C: FnOnce(Rc<P>) -> T,
    {
        let enter = self.span.take().unwrap().entered();

        self.in_tls.current_time = simulation.time;
        self.in_tls.current_task_id = TaskId::null();

        let context_lock = ContextLock::acquire(
            &mut self.context,
            simulation,
            &mut self.in_context, // if we open the context lock here, in_context shouldn't be accessible...
            &mut self.in_tls,
        );

        let result = f(self.protocol.clone());

        self.span = Some(enter.exit());
        drop(context_lock);

        result
    }

    pub fn process(&mut self, simulation: &mut SimulationInContext<P::Request, P::Response>) {
        // let _enter = span!(
        //     Level::INFO,
        //     "node",
        //     address = tracing::field::display(self.in_context.my_address)
        // )
        // .entered();

        let enter = self.span.take().unwrap().entered();

        // XXX: current time is part of the simulation, not executor
        //      perhaps the naming should change here...
        self.in_tls.current_time = simulation.time;

        self.process_messages(simulation);
        self.process_timeouts(simulation.time);
        self.process_sleeping_tasks(simulation.time);

        // if self.ready.is_empty() {
        //     trace!("EXECUTOR: nothing to do");
        //     return;
        // }

        for ready in self.ready.drain() {
            if let Some(task) = self.tasks.get_mut(ready) {
                self.in_tls.current_task_id = ready;

                let context_lock = ContextLock::acquire(
                    &mut self.context,
                    simulation,
                    &mut self.in_context, // if we open the context lock here, in_context shouldn't be accessible...
                    &mut self.in_tls,
                );

                let waker = taskid_waker(ready);
                let mut ctx = std::task::Context::from_waker(&waker);
                trace!("running task {}", ready);
                let task_finished = if task.poll(&mut ctx).is_ready() {
                    trace!("task {} is finished", ready);
                    self.tasks.remove(ready);
                    true
                } else {
                    false
                };

                drop(context_lock);

                simulation.stats.tasks_polled += 1;
                if task_finished {
                    simulation.stats.tasks_completed += 1;
                }
            } else {
                // trying to wake up non existent task?
            }
        }

        for task_id in self.in_tls.aborted.drain(..) {
            self.tasks.remove(task_id);
            trace!("task {} abort", task_id);
        }

        for task_id in self.in_tls.wake_up.drain(..) {
            self.ready.insert(task_id);
            trace!("task {} wake up", task_id);
        }

        for mut task in self.in_tls.spawned.drain(..) {
            let key = self.tasks.insert_with_key(|key| {
                task.set_id(key);
                task
            });
            self.ready.insert(key);
            trace!("spawn new task {}", key);
            simulation.stats.tasks_created += 1;
        }

        for (wake_up_time, task_id) in self.in_tls.sleep.drain(..) {
            self.sleep.push(SleepingTask::new(wake_up_time, task_id));
            trace!("task {} wake up at {}", task_id, wake_up_time);
        }

        self.span = Some(enter.exit());
    }
}

#[cfg(test)]
mod tests {
    use async_trait::async_trait;

    use std::collections::BinaryHeap;
    use std::future::Future;
    use std::rc::Rc;

    use crate::address::integer_to_address;
    use crate::context::Context;
    use crate::executor::Executor;
    use crate::futures::FuturesUnordered;
    use crate::proto::Protocol;
    use crate::rpc::RpcMessage;
    use crate::simulation::SimulationInContext;
    use crate::tls::{sleep, spawn};
    use crate::{current_time, oneshot, with_simulation_rng, SimulationConfig};
    use crate::{Address, Cookie};
    use futures_lite::future::yield_now;
    use futures_lite::StreamExt;
    use rand::rngs::StdRng;
    use rand::RngCore;
    use std::time::Duration;

    #[derive(Debug, Eq, PartialEq)]
    struct Req(u32);

    #[derive(Debug, Eq, PartialEq)]
    struct Ans(u32);

    struct TestProto;

    #[async_trait(?Send)]
    impl Protocol for TestProto {
        type Request = Req;
        type Response = Ans;
        type Config = ();

        fn create_data(
            _context: Context<Req, Ans>,
            _rng: &mut StdRng,
            _address: Address,
            _config: &(),
        ) -> Self {
            Self
        }

        async fn initialize(self: Rc<Self>, _: Context<Req, Ans>) {}

        async fn handle_request(
            self: &Rc<Self>,
            _: &Context<Req, Ans>,
            _: Address,
            request: Self::Request,
        ) -> Self::Response {
            Ans(request.0)
        }
    }

    fn get_request(simulation: &mut SimulationInContext<Req, Ans>) -> (Cookie, Req) {
        let req = simulation.bus.pop().unwrap();
        if let RpcMessage::Request(cookie, req) = req.message {
            (cookie, req)
        } else {
            panic!();
        }
    }

    fn get_response(simulation: &mut SimulationInContext<Req, Ans>) -> Ans {
        let req = simulation.bus.pop().unwrap();
        if let RpcMessage::Answer(_, ans) = req.message {
            ans
        } else {
            panic!();
        }
    }

    fn create_test_objects<'a>() -> (SimulationInContext<Req, Ans>, Executor<TestProto>) {
        let simulation = SimulationInContext {
            bus: BinaryHeap::new(),
            time: 0,
            config: SimulationConfig {
                n_nodes: 1,
                avg_death_time: None,
                ..Default::default()
            },
            stats: Default::default(),
        };
        let executor =
            Executor::<TestProto>::new(0, integer_to_address(0), &(), u32::MAX, u32::MAX);
        (simulation, executor)
    }

    fn run_my_task<F, T>(bootstrap: F)
    where
        F: Fn(Context<Req, Ans>) -> T,
        T: Future<Output = ()> + 'static,
    {
        let (mut simulation, mut executor) = create_test_objects();
        executor.spawn(bootstrap(executor.context()));
        executor.process(&mut simulation);
    }

    #[test]
    fn other() {
        run_my_task(|_| async move {
            println!("hello from task");
        });
    }

    #[test]
    fn oneshot() {
        let (mut simulation, mut executor) = create_test_objects();
        executor.spawn(async move {
            let (tx, rx) = oneshot::channel();

            spawn(async move {
                let result = rx.recv().await.unwrap();
                println!("got result: {}", result);
                assert_eq!("hello!", result);
            });

            spawn(async move {
                tx.send("hello!");
            });
        });
        executor.process_until_no_tasks(&mut simulation);
    }

    #[test]
    fn spawn_and_yield() {
        let (mut simulation, mut executor) = create_test_objects();
        executor.spawn(async move {
            println!("hello!");
            assert_eq!(current_time(), 0);
            spawn(async move {
                println!("hello from subtask!");
            });
            yield_now().await;
            assert_eq!(current_time(), 100);
            println!("bye!");
        });
        executor.process_until_no_tasks(&mut simulation);
    }

    #[test]
    fn spawn_join_handle() {
        let (mut simulation, mut executor) = create_test_objects();
        executor.spawn(async move {
            println!("hello!");
            let result = spawn(async move { "This came from a subtask!" }).await;
            println!("subtask says: {}", result);
        });
        executor.process_until_no_tasks(&mut simulation);
    }

    #[test]
    fn quick_sleep() {
        let (mut simulation, mut executor) = create_test_objects();

        executor.spawn(async move {
            let result = spawn(async move {
                sleep(Duration::from_secs(0)).await;
                "good"
            })
            .await;
            assert_eq!(result, "good");
        });

        executor.process_until_no_tasks(&mut simulation);
    }

    #[test]
    fn sleep_a_while() {
        let (mut simulation, mut executor) = create_test_objects();
        let (tx, rx) = std::sync::mpsc::channel();

        let indexes = vec![8, 3, 6, 1, 0, 9, 4, 2, 5, 7];

        executor.spawn(async move {
            for i in indexes {
                let tx = tx.clone();
                spawn(async move {
                    sleep(Duration::from_secs(i)).await;
                    tx.send(i).unwrap();
                });
            }
        });

        executor.process_until_no_tasks(&mut simulation);

        let items: Vec<u64> = rx.into_iter().collect();
        assert_eq!(items, vec![0, 1, 2, 3, 4, 5, 6, 7, 8, 9]);
    }

    #[test]
    fn sleep_a_while_futures_unordered() {
        let (mut simulation, mut executor) = create_test_objects();

        executor.spawn(async move {
            let mut expected = vec![8, 3, 6, 1, 0, 9, 4, 2, 5, 7];

            let mut futs = FuturesUnordered::new(expected.len());
            for i in expected.clone() {
                let handle = spawn(async move {
                    sleep(Duration::from_secs(i)).await;
                    println!("returning: {}", i);
                    i
                });
                futs.push(handle);
            }

            let mut actual = vec![];
            while let Some(result) = futs.next().await {
                println!("pushing: {}, size: {}", result, actual.len());
                actual.push(result);
            }

            expected.sort();
            assert_eq!(actual, expected);
        });

        executor.process_until_no_tasks(&mut simulation);
    }

    #[test]
    fn send_with_timeout() {
        let (mut simulation, mut executor) = create_test_objects();
        let context = executor.context();
        executor.spawn(async move {
            println!("sending request to address and awaiting...");
            let result = context.request(integer_to_address(1), Req(1)).await;
            println!("got result: {:?}", result);
            assert_eq!(current_time(), 5_000);
        });

        executor.process_until_no_tasks(&mut simulation);
    }

    #[test]
    fn send_with_response() {
        let (mut simulation, mut executor) = create_test_objects();
        let context = executor.context();
        executor.spawn(async move {
            let result = context.request(integer_to_address(1), Req(1)).await;
            println!("got result: {:?}", result);
        });

        executor.process(&mut simulation);

        // now, fake "reply" to that message
        let (cookie, req) = get_request(&mut simulation);
        executor.on_message_received(
            integer_to_address(1),
            RpcMessage::Answer(cookie, Ans(req.0)),
        );

        executor.process_until_no_tasks(&mut simulation);
    }

    #[test]
    fn handle_request() {
        let (mut simulation, mut executor) = create_test_objects();
        executor.on_message_received(integer_to_address(1), RpcMessage::Request(1, Req(666)));
        executor.on_message_received(integer_to_address(2), RpcMessage::Request(1, Req(1234)));
        executor.process_until_no_tasks(&mut simulation);

        let response = get_response(&mut simulation);
        assert!(response == Ans(666) || response == Ans(1234));

        let response = get_response(&mut simulation);
        assert!(response == Ans(666) || response == Ans(1234));
    }

    #[test]
    fn random() {
        let (mut simulation, mut executor) = create_test_objects();

        executor.spawn(async move {
            let random1 = with_simulation_rng(RngCore::next_u64);
            let random2 = with_simulation_rng(RngCore::next_u64);
            assert_ne!(random1, random2);
        });

        executor.process_until_no_tasks(&mut simulation);
    }

    #[test]
    fn seg_fault() {
        use assert_unmoved::AssertUnmoved;

        async fn nothing_future(msg: &'static str) -> &'static str {
            sleep(Duration::from_secs(1)).await;
            sleep(Duration::from_secs(1)).await;
            println!("returning: {}", msg);
            msg
        }

        let (mut simulation, mut executor) = create_test_objects();

        executor.spawn(async move {
            let mut futs = FuturesUnordered::new(3);
            futs.push(AssertUnmoved::new(nothing_future("1")));
            futs.push(AssertUnmoved::new(nothing_future("2")));
            futs.push(AssertUnmoved::new(nothing_future("3")));

            println!("{:?}", futs.next().await);
            println!("{:?}", futs.next().await);

            futs.push(AssertUnmoved::new(nothing_future("4")));

            println!("{:?}", futs.next().await);
            println!("{:?}", futs.next().await);
        });

        executor.process_until_no_tasks(&mut simulation);
    }

    #[test]
    fn abort_task_before_spawn() {
        let (mut simulation, mut executor) = create_test_objects();

        executor.spawn(async move {
            let (tx, rx) = oneshot::channel();

            let handle = spawn(async move {
                sleep(Duration::from_secs(1)).await;
                tx.send("should never happen");
                "cannot be returned"
            });
            handle.abort();

            yield_now().await;

            assert!(rx.recv().await.is_err());
            assert!(handle.is_finished());
        });

        executor.process_until_no_tasks(&mut simulation);
    }

    #[test]
    fn abort_task_after_spawn() {
        let (mut simulation, mut executor) = create_test_objects();

        executor.spawn(async move {
            let (tx, rx) = oneshot::channel();

            let handle = spawn(async move {
                sleep(Duration::from_secs(1)).await;
                tx.send("should never happen");
                "cannot be returned"
            });

            yield_now().await;

            handle.abort();

            yield_now().await;

            assert!(rx.recv().await.is_err());
            assert!(handle.is_finished());
        });

        executor.process_until_no_tasks(&mut simulation);
    }

    #[test]
    #[should_panic]
    fn abort_task_after_spawn_panic() {
        let (mut simulation, mut executor) = create_test_objects();

        executor.spawn(async move {
            let handle = spawn(async move {
                sleep(Duration::from_secs(1)).await;
                "cannot be returned"
            });

            yield_now().await;
            handle.abort();
            yield_now().await;

            let r = handle.await;
            println!("r: {}", r);
        });

        executor.process_until_no_tasks(&mut simulation);
    }
}
