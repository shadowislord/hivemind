use std::cell::Cell;
use std::fmt;
use std::future::Future;
use std::pin::Pin;
use std::rc::Rc;
use std::task::{Context, Poll};

use once_cell::unsync::OnceCell;

use crate::executor::TaskId;
use crate::tls::with_tls;

#[derive(Debug)]
pub(crate) struct Sender<T> {
    inner: Rc<Inner<T>>,
}

#[derive(Debug)]
struct Inner<T> {
    cell: OnceCell<T>,
    rx_task: Cell<Option<TaskId>>,
}

impl<T> Sender<T> {
    // XXX: we can scan the senders later to invoke callbacks
    pub fn send(&mut self, t: T) -> Option<TaskId> {
        assert!(self.inner.cell.set(t).is_ok(), "response already sent!");
        self.inner.rx_task.get()
    }

    pub fn get(&self) -> Option<&T> {
        self.inner.cell.get()
    }

    pub fn timeout(self) -> Option<TaskId> {
        self.inner.rx_task.get()
    }
}

#[derive(Debug)]
pub(crate) struct Receiver<T> {
    inner: Rc<Inner<T>>,
}

#[derive(Debug, Eq, PartialEq)]
pub(crate) struct RecvError;

impl fmt::Display for RecvError {
    fn fmt(&self, fmt: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(fmt, "channel closed")
    }
}

impl std::error::Error for RecvError {}

#[derive(Debug)]
#[must_use = "futures do nothing unless you `.await` or poll them"]
pub(crate) struct ReceiverFuture<T> {
    inner: Rc<Inner<T>>,
}

impl<T> Future for ReceiverFuture<T> {
    type Output = Result<T, RecvError>;

    fn poll(mut self: Pin<&mut Self>, _context: &mut Context<'_>) -> Poll<Self::Output> {
        if let Some(inner) = Rc::get_mut(&mut self.inner) {
            if let Some(result) = inner.cell.take() {
                Poll::Ready(Ok(result))
            } else {
                Poll::Ready(Err(RecvError))
            }
        } else {
            let current_task_id = with_tls(|tls| tls.current_task_id);
            self.inner.rx_task.set(Some(current_task_id));
            Poll::Pending
        }
    }
}

impl<T> Receiver<T> {
    pub fn recv(self) -> ReceiverFuture<T> {
        ReceiverFuture { inner: self.inner }
    }
}

pub(crate) fn channel<T>() -> (Sender<T>, Receiver<T>) {
    let inner = Rc::new(Inner {
        cell: OnceCell::new(),
        rx_task: Cell::new(None),
    });
    let tx = Sender {
        inner: inner.clone(),
    };
    let rx = Receiver { inner };
    (tx, rx)
}
