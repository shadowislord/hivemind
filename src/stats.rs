/// Simulation statistics
#[derive(Debug, Default)]
pub struct Statistics {
    /// Messages currently in transit.
    ///
    /// A message is in transit if it has been sent by a node
    /// but was not yet delivered to its destination due to latency.
    pub messages_in_transit: usize,

    /// Messages that were successfully delivered.
    ///
    /// A message is delivered if the destination node was
    /// alive at the time and was able to process the message.
    pub messages_delivered: usize,

    /// Messages that were lost due to packet loss.
    ///
    /// There's a random chance that a packet will be dropped, to simulate real network conditions.
    pub messages_lost: usize,

    /// Messages that bounced because the destination node did not exist.
    ///
    /// Either the destination node disconnected before it could receive the message
    /// or the message was sent to a non-existent address.
    pub messages_bounced: usize,

    /// How many nodes were killed.
    ///
    /// This is done via user code through the [`crate::Simulation::remove_node`] method.
    pub nodes_killed: usize,

    /// How many nodes the simulation was started with.
    ///
    /// See [`crate::config::SimulationConfig::n_nodes`].
    pub nodes_initial: usize,

    /// How many _additional_ nodes were created after the simulation was started.
    ///
    /// This does not include the nodes that the simulation started with ([`Self::nodes_initial`]).
    ///
    /// Additional nodes are randomly created to compensate for disconnecting nodes
    /// (as otherwise eventually the network will become empty).
    ///
    /// Nodes can also be created by the user through the [`crate::Simulation::add_node`] method.
    pub nodes_created: usize,

    /// How many nodes were disconnected.
    ///
    /// Because of network churn, nodes randomly join and leave the network,
    /// so some nodes will disconnect on their own.
    ///
    /// This can be configured via [`crate::config::SimulationConfig::avg_death_time`]
    pub nodes_disconnected: usize,

    /// How many nodes are currently alive on the network.
    pub nodes_current: usize,

    /// How many tasks were created.
    ///
    /// New tasks are created via [`crate::spawn`].
    pub tasks_created: usize,

    /// How many async tasks were polled.
    ///
    /// Every time a task makes progress, it is polled.
    pub tasks_polled: usize,

    /// How many async tasks were completed (were polled ready).
    pub tasks_completed: usize,

    /// The amount of time that has passed during the simulation
    pub time_passed: u32,
}
