use rand::rngs::StdRng;
use std::cell::UnsafeCell;
use std::convert::TryFrom;
use std::future::Future;
use std::pin::Pin;
use std::task::Poll;
use std::time::Duration;

use crate::executor::ExecutorThreadLocal;
use crate::join::{into_task_and_join_handle, JoinHandle};

/// Thread Local Context
struct ThreadLocal {
    inner: UnsafeCell<Option<&'static mut ExecutorThreadLocal>>,
}

impl ThreadLocal {
    fn new() -> Self {
        Self {
            inner: UnsafeCell::new(None),
        }
    }
}

thread_local! {
    static TLS: ThreadLocal = ThreadLocal::new();
}

pub(crate) fn load_tls(exec: &'static mut ExecutorThreadLocal) {
    TLS.with(move |tls| {
        let ptr = unsafe { &mut *tls.inner.get() };
        assert!(!ptr.is_some(), "context already loaded");
        *ptr = Some(exec);
    });
}

pub(crate) fn unload_tls() {
    TLS.with(|tls| {
        let ptr = unsafe { &mut *tls.inner.get() };
        assert!(!ptr.is_none(), "context lock already dropped");
        *ptr = None;
    });
}

pub(crate) fn with_tls<F, T>(f: F) -> T
where
    F: FnOnce(&mut ExecutorThreadLocal) -> T,
{
    TLS.with(|tls| {
        // Safety: parent must call unload_tls even if f() panics.
        if let Some(exec) = unsafe { &mut *tls.inner.get() } {
            f(exec)
        } else {
            panic!("no context current");
        }
    })
}

pub(crate) fn with_tls_option<F, T>(f: F) -> Option<T>
where
    F: FnOnce(&mut ExecutorThreadLocal) -> T,
{
    TLS.with(|tls| {
        // Safety: parent must call unload_tls even if f() panics.
        if let Some(exec) = unsafe { &mut *tls.inner.get() } {
            Some(f(exec))
        } else {
            None
        }
    })
}

/// Get the current task time
pub fn current_time() -> u32 {
    with_tls(|tls| tls.current_time)
}

/// Check if a valid context exists
pub fn is_context_valid() -> bool {
    with_tls_option(|_| ()).is_some()
}

/// Get simulation random number generator.
pub fn with_simulation_rng<T, F>(f: F) -> T
where
    F: FnOnce(&mut StdRng) -> T,
{
    with_tls(|tls| f(&mut tls.rng))
}

/// Spawn a new future on the runtime
pub fn spawn<T>(fut: impl Future<Output = T> + 'static) -> JoinHandle<T>
where
    T: 'static,
{
    let join_handle = with_tls(|tls| {
        let (task, join_handle) = into_task_and_join_handle(fut, tls.address);
        tls.spawned.push(task);
        join_handle
    });
    join_handle
}

/// Sleep future which is used for waiting some time
#[derive(Debug)]
pub enum SleepFuture {
    WaitForPoll(u32),
    Sleeping(u32),
}

impl Future for SleepFuture {
    type Output = ();

    fn poll(mut self: Pin<&mut Self>, _: &mut std::task::Context<'_>) -> Poll<Self::Output> {
        let current_time = current_time();
        match *self {
            SleepFuture::WaitForPoll(time) => {
                let wake_up_time = current_time + time;
                *self = SleepFuture::Sleeping(wake_up_time);
                with_tls(|tls| {
                    let task_id = tls.current_task_id;
                    tls.sleep.push((wake_up_time, task_id));
                });
                Poll::Pending
            }
            SleepFuture::Sleeping(wake_up_time) => {
                if current_time < wake_up_time {
                    with_tls(|tls| {
                        let task_id = tls.current_task_id;
                        tls.sleep.push((wake_up_time, task_id));
                    });
                    Poll::Pending
                } else {
                    Poll::Ready(())
                }
            }
        }
    }
}

/// Sleep for the given duration
///
/// (the sleep time is in simulation time, not real time)
pub fn sleep(duration: Duration) -> SleepFuture {
    if let Ok(sleep_time_ms) = u32::try_from(duration.as_millis()) {
        SleepFuture::WaitForPoll(sleep_time_ms)
    } else {
        panic!("duration {:?} is too long", duration);
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::address::integer_to_address;
    use rand::SeedableRng;

    #[test]
    fn happy_faces() {
        let mut exec = ExecutorThreadLocal::new(StdRng::seed_from_u64(0), integer_to_address(0));
        load_tls(unsafe { std::mem::transmute(&mut exec) });
        with_tls(|f| {
            assert!(f.spawned.is_empty());
        });
        unload_tls();
    }

    #[test]
    #[should_panic]
    fn load_tls_twice() {
        let mut exec = ExecutorThreadLocal::new(StdRng::seed_from_u64(0), integer_to_address(0));
        load_tls(unsafe { std::mem::transmute(&mut exec) });
        load_tls(unsafe { std::mem::transmute(&mut exec) });
    }

    #[test]
    #[should_panic]
    fn unload_tls_twice() {
        unload_tls();
        unload_tls();
    }

    #[test]
    #[should_panic]
    fn with_tls_without_tls() {
        with_tls(|_| {
            // do nothing
        });
    }

    #[test]
    #[should_panic]
    fn panic_in_tls() {
        let mut exec = ExecutorThreadLocal::new(StdRng::seed_from_u64(0), integer_to_address(0));
        load_tls(unsafe { std::mem::transmute(&mut exec) });
        with_tls(|_| {
            panic!();
        });
        unload_tls();
    }
}
