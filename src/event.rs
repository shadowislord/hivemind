use std::cmp::Ordering;
use std::fmt::{Debug, Formatter};

use crate::rpc::RpcMessage;
use crate::Address;

pub(crate) struct Event<R, A> {
    pub(crate) time: u32,
    pub(crate) src: Address,
    pub(crate) dst: Address,
    pub(crate) message: RpcMessage<R, A>,
}

impl<R: Debug, A: Debug> Debug for Event<R, A> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let cookie;
        let msg: &dyn Debug;
        let ty;
        match &self.message {
            RpcMessage::Request(req_cookie, req) => {
                ty = "req";
                cookie = req_cookie;
                msg = req;
            }
            RpcMessage::Answer(ans_cookie, ans) => {
                ty = "ans";
                cookie = ans_cookie;
                msg = ans;
            }
        };
        f.debug_struct("Event")
            .field("time", &self.time)
            .field("src", &format_args!("{}", self.src))
            .field("dst", &format_args!("{}", self.dst))
            .field("ty", &ty)
            .field("cookie", &cookie)
            .field("msg", &msg)
            .finish()
    }
}

impl<R, A> Event<R, A> {
    pub fn deliver(time: u32, src: Address, dst: Address, message: RpcMessage<R, A>) -> Self {
        Self {
            time,
            src,
            dst,
            message,
        }
    }
}

impl<R, A> PartialOrd for Event<R, A> {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        other.time.partial_cmp(&self.time)
    }
}

impl<R, A> Ord for Event<R, A> {
    fn cmp(&self, other: &Self) -> Ordering {
        other.time.cmp(&self.time)
    }
}

impl<R, A> PartialEq for Event<R, A> {
    fn eq(&self, other: &Self) -> bool {
        self.time.eq(&other.time)
    }
}

impl<R, A> Eq for Event<R, A> {}
