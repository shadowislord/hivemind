//! Contains the `JoinHandle`.

use once_cell::unsync::OnceCell;
use std::future::Future;
use std::pin::Pin;
use std::rc::Rc;
use std::task::Poll;

use crate::executor::TaskId;
use crate::oneshot::{Receiver, ReceiverFuture};
use crate::task::Task;
use crate::tls::with_tls;
use crate::{oneshot, Address};

#[derive(Debug)]
pub struct JoinHandle<T> {
    receiver: ReceiverFuture<T>,
    task_id_ref: Rc<OnceCell<TaskId>>,
    executor_address: Address,
}

impl<T> JoinHandle<T> {
    fn new(
        receiver: Receiver<T>,
        executor_address: Address,
        task_id_ref: Rc<OnceCell<TaskId>>,
    ) -> Self {
        Self {
            receiver: receiver.recv(),
            executor_address,
            task_id_ref,
        }
    }

    pub fn abort(&self) {
        with_tls(|tls| {
            assert_eq!(
                tls.address, self.executor_address,
                "JoinHandle can only be used by the node that created it"
            );
            if let Some(&id) = self.task_id_ref.get() {
                tls.aborted.push(id);
            } else {
                if let Some(p) = tls
                    .spawned
                    .iter()
                    .position(|task| task.is_same_id(&self.task_id_ref))
                {
                    tls.spawned.remove(p);
                } else {
                    panic!("cannot remove task which was created but not spawned(?)");
                }
            }
        });
    }

    pub fn is_finished(&self) -> bool {
        with_tls(|tls| {
            assert_eq!(
                tls.address, self.executor_address,
                "JoinHandle can only be used by the node that created it"
            );
        });
        self.receiver.is_finished()
    }
}

impl<T> Future for JoinHandle<T> {
    type Output = T;

    fn poll(mut self: Pin<&mut Self>, cx: &mut std::task::Context<'_>) -> Poll<Self::Output> {
        with_tls(|tls| {
            assert_eq!(
                tls.address, self.executor_address,
                "JoinHandle can only be used by the node that created it"
            );
        });
        Pin::new(&mut self.receiver).poll(cx).map(|result| {
            if let Ok(result) = result {
                result
            } else {
                panic!("task panicked or aborted");
            }
        })
    }
}

// pub(crate) fn into_tmp_task_and_join_handle<'a, T>(future: T) -> (TmpTask<'a>, JoinHandle<T::Output>)
//     where
//         T: Future + 'a,
//         T::Output: 'a,
// {
//     let (tx, rx) = oneshot::channel();
//     let join_handle = JoinHandle::new(rx);
//     let task = TmpTask::new_boxed(Box::pin(async move {
//         let result: T::Output = future.await;
//         tx.send(result);
//     }));
//     (task, join_handle)
// }

pub(crate) fn into_task_and_join_handle<T>(
    future: T,
    address: Address,
) -> (Task, JoinHandle<T::Output>)
where
    T: Future + 'static,
    T::Output: 'static,
{
    let (tx, rx) = oneshot::channel();
    let task = Task::new_boxed(Box::pin(async move {
        let result: T::Output = future.await;
        tx.send(result);
    }));
    let join_handle = JoinHandle::new(rx, address, task.get_id_ref());
    (task, join_handle)
}
