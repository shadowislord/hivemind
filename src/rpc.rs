use crate::Cookie;
use std::fmt;
use std::fmt::{Debug, Display};
use std::future::Future;
use std::pin::Pin;
use std::task::Poll;

use crate::oneshot_internal::Receiver;
use crate::oneshot_internal::ReceiverFuture;

/// No response was received in time
#[derive(Debug, Eq, PartialEq)]
pub struct Timeout;

impl Display for Timeout {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "timeout")
    }
}

impl std::error::Error for Timeout {}

#[derive(Debug)]
pub(crate) enum RpcMessage<R, A> {
    Request(Cookie, R),
    Answer(Cookie, A),
}

pub struct RequestFuture<T> {
    receiver: ReceiverFuture<T>,
}

impl<T> RequestFuture<T> {
    pub(crate) fn new(receiver: Receiver<T>) -> Self {
        Self {
            receiver: receiver.recv(),
        }
    }
}

impl<T> Future for RequestFuture<T> {
    type Output = Result<T, Timeout>;

    fn poll(mut self: Pin<&mut Self>, cx: &mut std::task::Context<'_>) -> Poll<Self::Output> {
        Pin::new(&mut self.receiver).poll(cx).map_err(|_| Timeout)
    }
}
