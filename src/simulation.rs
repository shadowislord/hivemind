use once_cell::sync::OnceCell;
use rand::distributions::OpenClosed01;
use rand::rngs::StdRng;
use rand::{Rng, RngCore, SeedableRng};
use std::cell::RefCell;
use std::collections::{BinaryHeap, HashMap};
use std::fmt::{Debug, Display, Formatter};
use std::future::Future;
use std::rc::Rc;

use tracing::{info, trace};

use crate::address_generator::AddressGenerator;
use crate::buildhasher::SimHashMap;
use crate::event::Event;
use crate::executor::Executor;
use crate::proto::Protocol;
use crate::stats::Statistics;
use crate::time::SimulationTimer;
use crate::utils::{BinaryHeapExt, RngExt};
use crate::{Address, SimulationConfig};

#[derive(Debug)]
pub(crate) struct SimulationInContext<R, A> {
    pub(crate) bus: BinaryHeap<Event<R, A>>,
    pub(crate) time: u32,
    pub(crate) config: SimulationConfig,
    pub(crate) stats: Statistics,
}

/// This is how the world is simulated.
///
/// We create a certain number of nodes and then kick off the simulation.
#[derive(Debug)]
pub struct Simulation<P: Protocol> {
    in_context: SimulationInContext<P::Request, P::Response>,
    address_generator: AddressGenerator,
    executors: SimHashMap<Address, Executor<P>>,
    exec_rng: StdRng,
    api_rng: RefCell<StdRng>,
    next_spawn_time: Option<u32>,
    proto_cfg: P::Config,
}

impl<P: Protocol + Debug> Display for Simulation<P>
where
    P::Config: Debug,
{
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let mut map = HashMap::with_capacity(self.executors.len());
        for (k, v) in self.executors.iter() {
            map.insert(k, v);
        }
        f.debug_struct("Simulation")
            .field("nodes", &map)
            .field("exec_rng", &self.exec_rng)
            .field("api_rng", &self.api_rng.borrow())
            .field("time", &self.in_context.time)
            .field("tick_time", &self.in_context.config.tick_time)
            .field("timeout", &self.in_context.config.timeout)
            .field("proto_cfg", &self.proto_cfg)
            .finish()
    }
}

impl<P: Protocol> Simulation<P> {
    /// Create a new simulation with the given configuration
    pub fn new(sim_cfg: SimulationConfig, proto_cfg: P::Config) -> Self {
        let mut exec_rng = StdRng::seed_from_u64(sim_cfg.initial_seed);
        info!("simulation init, n_nodes = {}", sim_cfg.n_nodes);

        let mut address_generator = AddressGenerator::default();
        let mut executors =
            SimHashMap::with_capacity_and_hasher(sim_cfg.n_nodes, Default::default());
        let mut addresses = Vec::with_capacity(sim_cfg.n_nodes);
        for _ in 0..sim_cfg.n_nodes {
            let key = address_generator.generate(&mut exec_rng);
            executors.insert(
                key,
                Self::create_executor(&mut exec_rng, 0, key, &sim_cfg, &proto_cfg),
            );
            addresses.push(key);
        }

        // Cannot use an iterator here because we need to be able to access friends
        for node_index in 0..sim_cfg.n_nodes {
            let node_address = addresses[node_index];
            let executor = executors.get_mut(&node_address).unwrap();

            let mut friends = Vec::with_capacity(sim_cfg.num_friends);
            if sim_cfg.n_nodes > 1 {
                for _ in 0..friends.capacity() {
                    let friend_index = exec_rng.usize_except(sim_cfg.n_nodes, node_index);
                    friends.push(addresses[friend_index]);
                }
            }
            executor.set_friends(friends);
        }

        let next_spawn_time = if sim_cfg.avg_death_time.is_some() {
            Some(Self::generate_spawn_time(&mut exec_rng, &sim_cfg))
        } else {
            None
        };

        let api_rng = StdRng::seed_from_u64(sim_cfg.initial_seed.wrapping_add(1));

        let mut in_context = SimulationInContext {
            bus: BinaryHeap::new(),
            config: sim_cfg,
            time: 0,
            stats: Statistics::default(),
        };

        in_context.stats.nodes_initial += executors.len();

        Self {
            in_context,
            address_generator,
            executors,
            exec_rng,
            api_rng: RefCell::new(api_rng),
            next_spawn_time,
            proto_cfg,
        }
    }

    fn create_executor(
        rng: &mut StdRng,
        current_time: u32,
        address: Address,
        sim_cfg: &SimulationConfig,
        proto_cfg: &P::Config,
    ) -> Executor<P> {
        let rng_seed = rng.next_u64();
        let death_time = current_time + Self::generate_death_time(rng, sim_cfg);
        Executor::new(rng_seed, address, proto_cfg, current_time, death_time)
    }

    fn generate_death_time(rng: &mut StdRng, config: &SimulationConfig) -> u32 {
        if let Some(avg_death_time) = config.avg_death_time {
            (rng.f32_gaussian().abs() * avg_death_time as f32) as u32
        } else {
            u32::MAX
        }
    }

    fn generate_spawn_time(rng: &mut StdRng, config: &SimulationConfig) -> u32 {
        if let Some(avg_death_time) = config.avg_death_time {
            let avg_death_time = avg_death_time as f32;
            let n_nodes = config.n_nodes as f32;
            let spawn_time = (rng.f32_gaussian().abs() * avg_death_time) / n_nodes;
            spawn_time as u32
        } else {
            u32::MAX
        }
    }

    /// Returns the address of a random node, or None if no nodes exist
    pub fn random_node(&self) -> Option<Address> {
        // TODO: we want this to be predictable but at the same time not break
        //       the determinism of the simulation
        let mut keys = self.executors.keys();
        let node_index = self.api_rng.borrow_mut().gen_range(0..keys.len());
        keys.nth(node_index).copied()
    }

    /// Adds a new node into the network, returning its address.
    pub fn add_node(&mut self) -> Address {
        let current_time = self.in_context.time;

        let mut friends = Vec::with_capacity(self.in_context.config.num_friends);
        for _ in 0..friends.capacity() {
            let addr = self.random_node().unwrap();
            friends.push(addr);
        }

        let address = self.address_generator.generate(&mut self.exec_rng);
        let mut executor = Self::create_executor(
            &mut self.exec_rng,
            current_time,
            address,
            &self.in_context.config,
            &self.proto_cfg,
        );
        executor.set_friends(friends);

        assert!(self.executors.insert(address, executor).is_none());

        trace!("created node {}", address);
        self.in_context.stats.nodes_created += 1;

        address
    }

    /// Removes a node from the network.
    ///
    /// If the given address exists, the node is removed and true is returned, false otherwise.
    pub fn remove_node(&mut self, address: Address) -> bool {
        if let Some(mut executor) = self.executors.remove(&address) {
            executor.disconnect();
            self.in_context.stats.nodes_killed += 1;
            true
        } else {
            false
        }
    }

    /// Prevents the given node from dying naturally.
    ///
    /// [`Self::remove_node`] can still be used to kill the node forcibly.
    pub fn protect_node(&mut self, address: Address) -> bool {
        if let Some(executor) = self.executors.get_mut(&address) {
            executor.protect();
            true
        } else {
            false
        }
    }

    /// Obtain statistics about the simulation.
    pub fn stats(&self) -> &Statistics {
        &self.in_context.stats
    }

    /// Returns the time when the node was created.
    pub fn birth_time(&self, address: Address) -> Option<u32> {
        self.executors.get(&address).map(Executor::birth_time)
    }

    pub fn run_on<C, T>(&mut self, address: Address, f: C) -> T
    where
        C: FnOnce(Rc<P>) -> T,
    {
        let executor = self
            .executors
            .get_mut(&address)
            .unwrap_or_else(|| panic!("node doesn't exist: {}", address));

        executor.run_on(&mut self.in_context, f)
    }

    /// Run the given async closure inside the simulation, against the given node address, waiting for it to finish.
    ///
    /// The simulation will continue to run as long as the future returned by the closure is pending.
    /// When the future is ready, its' result will be returned.
    ///
    /// # Panics
    ///
    /// * If the given address does not exist
    /// * If the future blocks for more than 50 seconds (in simulated time).
    pub fn block_on<C, F, T>(&mut self, address: Address, f: C) -> T
    where
        C: FnOnce(Rc<P>) -> F + 'static,
        F: Future<Output = T> + 'static,
        T: 'static,
    {
        let executor = self
            .executors
            .get_mut(&address)
            .unwrap_or_else(|| panic!("node doesn't exist: {}", address));

        let protocol = executor.protocol().clone();

        let mut cell: Rc<OnceCell<T>> = Rc::new(OnceCell::new());
        let cell_clone = cell.clone();

        let b = Box::pin(async move {
            let result = f(protocol).await;
            if cell_clone.set(result).is_err() {
                unreachable!();
            }
        });

        executor.spawn_box(b);

        for _ in 0..2 * 60 * 10 {
            // approx 50s
            self.tick();
            if let Some(inner) = Rc::get_mut(&mut cell) {
                if let Some(result) = inner.take() {
                    return result;
                }
            }
        }

        panic!("did not get result within timeout");
    }

    pub fn first_node(&mut self) -> &P {
        let key = *self.executors.keys().next().unwrap();
        self.executors.get_mut(&key).unwrap().protocol().as_ref()
    }

    pub fn first_node_address(&self) -> Address {
        *self.executors.keys().next().unwrap()
    }

    pub fn node(&self, address: Address) -> Option<&P> {
        if let Some(executor) = self.executors.get(&address) {
            Some(executor.protocol().as_ref())
        } else {
            None
        }
    }

    pub fn nodes(&self) -> impl Iterator<Item = (Address, &P)> {
        self.executors
            .iter()
            .map(|(address, executor)| (*address, executor.protocol().as_ref()))
    }

    // fn enter_node_context(address: Address, executor: &Executor<P>) -> &Span {
    // span!(
    //     Level::INFO,
    //     "node",
    //     id = tracing::field::display(executor.node_name()),
    //     address = tracing::field::display(address.ip()),
    // )
    // }

    pub fn time(&self) -> u32 {
        self.in_context.time
    }

    pub fn tick(&mut self) {
        self.in_context.stats.time_passed = self.in_context.time;

        let next_tick = self.in_context.time + self.in_context.config.tick_time;

        SimulationTimer::set_time(self.in_context.time);

        let mut nodes_to_spawn = 0_u32;
        if let Some(mut next_spawn_time) = self.next_spawn_time {
            while next_spawn_time <= self.in_context.time {
                nodes_to_spawn += 1;
                next_spawn_time +=
                    Self::generate_spawn_time(&mut self.exec_rng, &self.in_context.config);
            }
            self.next_spawn_time = Some(next_spawn_time);
        }

        while let Some(event) = self.in_context.bus.pop_if(|e| e.time <= next_tick) {
            if let Some(exec) = self.executors.get_mut(&event.dst) {
                if self.exec_rng.sample::<f32, _>(OpenClosed01)
                    > self.in_context.config.packet_loss_rate
                {
                    exec.on_message_received(event.src, event.message);
                    self.in_context.stats.messages_delivered += 1;
                } else {
                    trace!("randomly dropping packet destined for node {}", event.dst);
                    self.in_context.stats.messages_lost += 1;
                }
            } else {
                trace!(
                    "dropping undeliverable message, since node {} is down",
                    event.dst
                );
                self.in_context.stats.messages_bounced += 1;
            }
        }

        self.executors.retain(|_address, executor| {
            if executor.death_time() > self.in_context.time {
                executor.process(&mut self.in_context);
                true
            } else {
                // let _span = Self::enter_node_context(*address, executor).enter();
                executor.disconnect();
                self.in_context.stats.nodes_disconnected += 1;
                false
            }
        });

        for _ in 0..nodes_to_spawn {
            self.add_node();
        }

        self.in_context.stats.nodes_current = self.executors.len();
        self.in_context.stats.messages_in_transit = self.in_context.bus.len();

        self.in_context.time = next_tick;
    }
}

#[cfg(test)]
mod tests {
    use std::cell::Cell;
    use std::rc::Rc;
    use std::time::Duration;

    use async_trait::async_trait;
    use futures_lite::future::yield_now;
    use rand::rngs::StdRng;
    use rand::RngCore;
    use tracing::info;

    use crate::address::integer_to_address;
    use crate::context::Context;
    use crate::proto::Protocol;
    use crate::simulation::{Simulation, SimulationConfig};
    use crate::tls::spawn;
    use crate::{sleep, with_simulation_rng, Address};

    #[derive(Debug)]
    struct Req;

    #[derive(Debug)]
    struct Ans;

    #[derive(Debug)]
    struct TestProto;

    impl TestProto {
        async fn do_something(self: Rc<Self>, a: u32, b: u32) -> u32 {
            sleep(Duration::from_millis(500)).await;
            a + b
        }
    }

    #[async_trait(? Send)]
    impl Protocol for TestProto {
        type Request = Req;
        type Response = Ans;
        type Config = u32;

        fn create_data(
            _context: Context<Req, Ans>,
            _rng: &mut StdRng,
            _address: Address,
            config: &u32,
        ) -> Self {
            assert_eq!(*config, 666);
            Self
        }

        async fn initialize(self: Rc<Self>, context: Context<Req, Ans>) {
            info!("hello world!");
            let result = context.request(context.address(), Req).await;
            info!("result: {:?}", result);
        }

        async fn handle_request(
            self: &Rc<Self>,
            _: &Context<Req, Ans>,
            _: Address,
            _: Self::Request,
        ) -> Self::Response {
            Ans
        }
    }

    fn create_test_config(n_nodes: usize) -> SimulationConfig {
        SimulationConfig {
            initial_seed: 0,
            n_nodes,
            timeout: 1000,
            tick_time: 100,
            avg_death_time: None,
            num_friends: 3,
            packet_loss_rate: 0.,
        }
    }

    #[test]
    fn test() {
        let mut simulation = Simulation::<TestProto>::new(create_test_config(2), 666);
        for _ in 0..=100 {
            info!("calling TICK!");
            simulation.tick();
        }
    }

    #[test]
    fn random_node_is_random() {
        let simulation = Simulation::<TestProto>::new(create_test_config(1000), 666);
        let n1 = simulation.random_node().unwrap();
        let n2 = simulation.random_node().unwrap();
        assert_ne!(n1, n2);
    }

    #[test]
    fn block_on() {
        let mut simulation = Simulation::<TestProto>::new(create_test_config(2), 666);
        let address = simulation.random_node().unwrap();
        let result = simulation.block_on(address, |p| p.do_something(2, 2));
        assert_eq!(result, 4);
        assert_eq!(simulation.time(), 600);
    }

    #[test]
    fn add_node() {
        let mut simulation = Simulation::<TestProto>::new(create_test_config(1), 666);
        simulation.add_node();
        simulation.tick();
    }

    #[test]
    fn remove_node() {
        let mut simulation = Simulation::<TestProto>::new(create_test_config(2), 666);
        simulation.remove_node(integer_to_address(0));
        simulation.tick();
        println!("{:#}", simulation);
    }

    #[test]
    fn run_on() {
        let mut simulation = Simulation::<TestProto>::new(create_test_config(2), 666);
        let addr = simulation.random_node().unwrap();

        let random = simulation.run_on(addr, |_p| with_simulation_rng(|r| r.next_u32()));

        assert_eq!(random, 736657898);
    }

    struct Test2Proto {
        did_get_response: Cell<bool>,
        hook_called: Cell<bool>,
    }

    #[async_trait(? Send)]
    impl Protocol for Test2Proto {
        type Request = String;
        type Response = String;
        type Config = ();

        fn create_data(
            _context: Context<String, String>,
            _rng: &mut StdRng,
            _address: Address,
            _config: &(),
        ) -> Self {
            Self {
                did_get_response: Cell::new(false),
                hook_called: Cell::new(false),
            }
        }

        async fn initialize(self: Rc<Self>, context: Context<String, String>) {
            let first_friend = context.friends()[0];

            assert!(!self.hook_called.get());

            let self_clone1 = self.clone();
            let ctx1 = context.clone();
            let handle = spawn(async move {
                let response = ctx1
                    .request(first_friend, "How are you".to_owned())
                    .await
                    .unwrap();
                info!("the response was: {}", response);
                self_clone1.did_get_response.set(true);
            });

            let self_clone2 = self.clone();
            spawn(async move {
                let _ = handle.await;
                assert!(self_clone2.did_get_response.get());
                assert!(self_clone2.hook_called.get());
                info!("things are looking great from here");
            });

            let time = context.time();
            yield_now().await;
            assert!(context.time() > time);
        }

        async fn handle_request(
            self: &Rc<Self>,
            _: &Context<String, String>,
            _: Address,
            _: Self::Request,
        ) -> Self::Response {
            "I'm okay".to_owned()
        }

        fn on_response_received(
            self: &Rc<Self>,
            _ctx: &Context<String, String>,
            _address: Address,
            _response: &Self::Response,
        ) {
            self.hook_called.set(true);
        }
    }

    #[test]
    fn end_to_end() {
        let mut simulation = Simulation::<Test2Proto>::new(create_test_config(2), ());
        simulation.tick();
        simulation.tick();
        simulation.tick();
        simulation.tick();
        simulation.tick();
        simulation.tick();
        simulation.tick();
        simulation.tick();
    }
}
