/// Simulation configuration
#[derive(Debug)]
pub struct SimulationConfig {
    /// The initial seed for the random number generator
    ///
    /// Default: 0
    pub initial_seed: u64,

    /// The number of nodes to create the network with
    ///
    /// Default: 2 nodes
    pub n_nodes: usize,

    /// Timeout in milliseconds.
    ///
    /// Packets not delivered in time will cause a [`crate::rpc::Timeout`] error.
    ///
    /// Default: 5000 milliseconds.
    pub timeout: u32,

    /// How much time passes in a single tick in milliseconds.
    ///
    /// During a tick, all nodes will be processed (packets will be delivered, sleeps/yields will be resolved, etc)
    ///
    /// Default: 100 milliseconds.
    pub tick_time: u32,

    /// The average death time in milliseconds.
    ///
    /// On average, nodes will die after this amount of time.
    ///
    /// Default: 60,000 milliseconds.
    pub avg_death_time: Option<u32>,

    /// Number of friends that each node will be given.
    ///
    /// Default: 3
    pub num_friends: usize,

    /// The probability that a packet will be dropped.
    ///
    /// Default: 0.05
    pub packet_loss_rate: f32,
}

impl SimulationConfig {
    pub fn for_test() -> Self {
        Self {
            avg_death_time: None,
            packet_loss_rate: 0.0,
            ..Default::default()
        }
    }

    pub fn n_nodes(mut self, n_nodes: usize) -> Self {
        self.n_nodes = n_nodes;
        self
    }
}

impl Default for SimulationConfig {
    fn default() -> Self {
        Self {
            initial_seed: 0,
            n_nodes: 2,
            timeout: 5_000,
            tick_time: 100,
            avg_death_time: Some(60_000),
            num_friends: 3,
            packet_loss_rate: 0.05,
        }
    }
}
