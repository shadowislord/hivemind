use std::cell::UnsafeCell;
use std::fmt::Debug;
use std::marker::{PhantomData, PhantomPinned};
use std::rc::Rc;

use rand::Rng;
use tracing::trace;

use crate::buildhasher::SimHashMap;
use crate::event::Event;
use crate::executor::{ExecutorInContext, ExecutorThreadLocal};
use crate::oneshot_internal::Sender;
use crate::rpc::{RequestFuture, RpcMessage, Timeout};
use crate::simulation::SimulationInContext;
use crate::tls::{load_tls, unload_tls};
use crate::utils::RngExt;
use crate::{oneshot_internal, Address, Cookie};

struct Inner<R: 'static, A: 'static> {
    executor: &'static mut ExecutorInContext<A>,
    simulation: &'static mut SimulationInContext<R, A>,
    _pinned: PhantomPinned,
}

/// The context allows us to interact with the rest of the system inside of the simulated protocol.
#[derive(Debug)]
pub struct Context<R: 'static, A: 'static> {
    ptr: Rc<UnsafeCell<Option<Inner<R, A>>>>,
}

impl<R, A> Clone for Context<R, A> {
    fn clone(&self) -> Self {
        Self {
            ptr: self.ptr.clone(),
        }
    }
}

impl<R: Debug, A: Debug> Context<R, A> {
    pub(crate) fn new() -> Self {
        Self {
            ptr: Rc::new(UnsafeCell::new(None)),
        }
    }

    #[allow(clippy::mut_from_ref)]
    fn inner(&self) -> &mut Inner<R, A> {
        let ptr = unsafe { &mut *self.ptr.get() };
        if let Some(ptr) = ptr {
            ptr
        } else {
            panic!("no context current");
        }
    }

    /// The (predictable) random number generator
    #[inline]
    pub fn rng(&self) -> &mut impl Rng {
        let inner = self.inner();
        &mut inner.executor.rng
    }

    /// The current time in milliseconds.
    #[inline]
    pub fn time(&self) -> u32 {
        let inner = self.inner();
        inner.simulation.time
    }

    /// Address of this node.
    #[inline]
    pub fn address(&self) -> Address {
        let inner = self.inner();
        inner.executor.my_address
    }

    /// Friends of this node that we can contact to find out more about the network.
    #[inline]
    pub fn friends(&self) -> &[Address] {
        let inner = self.inner();
        &inner.executor.friends
    }

    // TODO: configurable latency
    fn simulate_latency(inner: &mut Inner<R, A>) -> u32 {
        let latency = 10_f32 + inner.executor.rng.f32_gaussian().abs() * 200_f32;
        latency as u32
    }

    // TODO: this doesn't belong here
    pub(crate) fn respond(&self, address: Address, cookie: Cookie, answer: A) {
        let inner = self.inner();

        trace!(
            "{} is sending {:?} to {}",
            inner.executor.my_address,
            answer,
            address
        );

        let latency = Self::simulate_latency(inner);
        let deliver_time = inner.simulation.time + latency;

        let response = RpcMessage::Answer(cookie, answer);
        let event = Event::deliver(deliver_time, inner.executor.my_address, address, response);

        inner.simulation.bus.push(event);
    }

    // TODO: this doesn't belong here
    pub(crate) fn pending(&mut self) -> &mut SimHashMap<(Address, Cookie), Sender<A>> {
        let inner = self.inner();
        &mut inner.executor.pending
    }

    /// Send a request to a node, and wait for a response.
    pub async fn request(&self, address: Address, request: R) -> Result<A, Timeout> {
        let inner = self.inner();

        let cookie = inner.executor.next_cookie;
        inner.executor.next_cookie = cookie.checked_add(1).expect("ran out of cookies");

        trace!(
            "{} is sending {:?} to {}",
            inner.executor.my_address,
            request,
            address
        );

        let latency = Self::simulate_latency(inner);
        let deliver_time = inner.simulation.time + latency;

        let request = RpcMessage::Request(cookie, request);
        let event = Event::deliver(deliver_time, inner.executor.my_address, address, request);

        let (tx, rx) = oneshot_internal::channel();

        inner.simulation.bus.push(event);
        inner.executor.pending.insert((address, cookie), tx);
        inner.executor.timeouts.push_back((
            address,
            cookie,
            inner.simulation.time + inner.simulation.config.timeout,
        ));

        RequestFuture::new(rx).await
    }
}

pub(crate) struct ContextLock<'a, R: 'static, A: 'static> {
    ptr: Rc<UnsafeCell<Option<Inner<R, A>>>>,
    _lifetime: PhantomData<&'a ()>,
}

impl<'a, R, A> ContextLock<'a, R, A> {
    pub fn acquire(
        ctx: &mut Context<R, A>,
        simulation: &'a mut SimulationInContext<R, A>,
        executor: &'a mut ExecutorInContext<A>,
        tls: &'a mut ExecutorThreadLocal,
    ) -> ContextLock<'a, R, A> {
        let ptr = unsafe { &mut *ctx.ptr.get() };
        assert!(!ptr.is_some(), "context is already set");
        let inner = Inner {
            executor: unsafe { std::mem::transmute(executor) },
            simulation: unsafe { std::mem::transmute(simulation) },
            _pinned: PhantomPinned,
        };
        load_tls(unsafe { std::mem::transmute(tls) });
        *ptr = Some(inner);
        Self {
            ptr: ctx.ptr.clone(),
            _lifetime: PhantomData,
        }
    }
}

impl<'a, R, A> Drop for ContextLock<'a, R, A> {
    fn drop(&mut self) {
        unload_tls();
        let ptr = unsafe { &mut *self.ptr.get() };
        assert!(!ptr.is_none(), "context lock already dropped");
        *ptr = None;
    }
}
