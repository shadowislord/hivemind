# Hivemind

A peer to peer protocol simulator written in Rust

## Description

Hivemind is a library that can be used to simulate a peer to peer protocol.

A virtual network is created containing nodes with virtual addresses. Nodes can communicate with each other on the network. 

## Features

* Upwards of 10,000 nodes can be simulated in almost near time. The library is very light-weight, so memory and CPU usage should be minimal. It should be possible to simulate large networks on commodity hardware
* Network conditions such as latency, packet loss, and nodes joining / leaving the network can be simulated
* Asynchronous Rust is used to implement the protocol communication, which simplifies usage
* APIs are provided to make implementing protocols easier, such as yield, sleep, get current (simulated) time, etc

## Usage

Currently the library requires nightly Rust to use

TODO...

## License

The library is released under the GNU AGPLv3 license.

## Project status

For the most part the library is feature complete. There are no plans to make major changes at this point.
