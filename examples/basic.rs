use async_trait::async_trait;
use hivemind::{Address, Context, Protocol, Simulation, SimulationConfig};
use rand::rngs::StdRng;
use std::rc::Rc;
use tracing::{info, Level};
use tracing_subscriber::fmt::format::FmtSpan;

struct TestProto;

#[async_trait(?Send)]
impl Protocol for TestProto {
    type Request = ();
    type Response = ();
    type Config = ();

    fn create_data(
        _context: Context<(), ()>,
        _rng: &mut StdRng,
        _address: Address,
        _config: &(),
    ) -> Self {
        Self
    }

    async fn initialize(self: Rc<Self>, context: Context<(), ()>) {
        info!("init!");
        context.request(context.friends()[0], ()).await.unwrap();
    }

    async fn handle_request(
        self: &Rc<Self>,
        _: &Context<(), ()>,
        _: Address,
        _: Self::Request,
    ) -> Self::Response {
        info!("handle request!");
    }

    fn on_response_received(
        self: &Rc<Self>,
        _context: &Context<(), ()>,
        address: Address,
        response: &Self::Response,
    ) {
        info!("response received: {:?} from {}", response, address);
    }
}

fn main() {
    tracing_subscriber::fmt()
        .with_max_level(Level::INFO)
        .with_span_events(FmtSpan::NONE)
        .init();

    let cfg = SimulationConfig {
        n_nodes: 2,
        avg_death_time: None,
        packet_loss_rate: 0.0,
        ..Default::default()
    };

    let mut sim = Simulation::<TestProto>::new(cfg, ());
    sim.tick();
    sim.tick();
    sim.tick();
    sim.tick();
}
